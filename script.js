const images = document.querySelectorAll(".image-to-show");
const start = document.querySelector("#start");
const stop = document.querySelector("#stop");
let currentIndex = 0;
let timeoutId;

const nextImage = () => {
    images[currentIndex].style.display = "none";
    currentIndex = (currentIndex + 1) % images.length ;
    images[currentIndex].style.display = "block";
    timeoutId = setTimeout(nextImage , 3000);
};
    timeoutId = setTimeout(nextImage , 3000);

stop.addEventListener("click", () => {
    clearTimeout(timeoutId);
});

start.addEventListener("click", () => {
    timeoutId = setTimeout(nextImage, 3000);
});